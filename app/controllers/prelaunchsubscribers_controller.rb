class PrelaunchsubscribersController < ApplicationController
  before_action :set_prelaunchsubscriber, only: [:show, :edit, :update, :destroy]

  # GET /prelaunchsubscribers
  # GET /prelaunchsubscribers.json
  def index
    @prelaunchsubscribers = Prelaunchsubscriber.all
  end

  # GET /prelaunchsubscribers/1
  # GET /prelaunchsubscribers/1.json
  def show
  end

  # GET /prelaunchsubscribers/new
  def new
    @prelaunchsubscriber = Prelaunchsubscriber.new
  end

  # GET /prelaunchsubscribers/1/edit
  def edit
  end

  # POST /prelaunchsubscribers
  # POST /prelaunchsubscribers.json
  def create
    @prelaunchsubscriber = Prelaunchsubscriber.new(prelaunchsubscriber_params)

    respond_to do |format|
      if @prelaunchsubscriber.save
        format.html { redirect_to @prelaunchsubscriber, notice: 'Prelaunchsubscriber was successfully created.' }
        format.json { render :show, status: :created, location: @prelaunchsubscriber }
        format.js
      else
        format.html { render :new }
        format.json { render json: @prelaunchsubscriber.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /prelaunchsubscribers/1
  # PATCH/PUT /prelaunchsubscribers/1.json
  def update
    respond_to do |format|
      if @prelaunchsubscriber.update(prelaunchsubscriber_params)
        format.html { redirect_to @prelaunchsubscriber, notice: 'Prelaunchsubscriber was successfully updated.' }
        format.json { render :show, status: :ok, location: @prelaunchsubscriber }
      else
        format.html { render :edit }
        format.json { render json: @prelaunchsubscriber.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /prelaunchsubscribers/1
  # DELETE /prelaunchsubscribers/1.json
  def destroy
    @prelaunchsubscriber.destroy
    respond_to do |format|
      format.html { redirect_to prelaunchsubscribers_url, notice: 'Prelaunchsubscriber was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_prelaunchsubscriber
      @prelaunchsubscriber = Prelaunchsubscriber.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def prelaunchsubscriber_params
      # params.fetch(:prelaunchsubscriber, {:emailid => :emailid})
      params.required(:prelaunchsubscriber).permit(:emailid)
    end
end
