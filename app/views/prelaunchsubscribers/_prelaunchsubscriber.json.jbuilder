json.extract! prelaunchsubscriber, :id, :created_at, :updated_at
json.url prelaunchsubscriber_url(prelaunchsubscriber, format: :json)