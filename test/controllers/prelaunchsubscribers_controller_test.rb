require 'test_helper'

class PrelaunchsubscribersControllerTest < ActionController::TestCase
  setup do
    @prelaunchsubscriber = prelaunchsubscribers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:prelaunchsubscribers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create prelaunchsubscriber" do
    assert_difference('Prelaunchsubscriber.count') do
      post :create, prelaunchsubscriber: {  }
    end

    assert_redirected_to prelaunchsubscriber_path(assigns(:prelaunchsubscriber))
  end

  test "should show prelaunchsubscriber" do
    get :show, id: @prelaunchsubscriber
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @prelaunchsubscriber
    assert_response :success
  end

  test "should update prelaunchsubscriber" do
    patch :update, id: @prelaunchsubscriber, prelaunchsubscriber: {  }
    assert_redirected_to prelaunchsubscriber_path(assigns(:prelaunchsubscriber))
  end

  test "should destroy prelaunchsubscriber" do
    assert_difference('Prelaunchsubscriber.count', -1) do
      delete :destroy, id: @prelaunchsubscriber
    end

    assert_redirected_to prelaunchsubscribers_path
  end
end
