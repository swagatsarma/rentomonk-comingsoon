class CreatePrelaunchsubscribers < ActiveRecord::Migration
  def change
    create_table :prelaunchsubscribers do |t|
    	t.string :emailid, null: false
      t.timestamps null: false
    end
  end
end
